export default {
  attributeAuthentication: {
    tokenUser: null,
    login: false,
    username: null,
    roleUser: null
  },

  /*
   * Di bawah ini adalah attribute atau variable yang di butuhkan untuk kebutuhan layouts aplikasi
   * */
  attributeLayouts: {
    login: {
      ErrorComponents: {
        DialogError: {
          show: false,
          TitleDialog: null,
          TextContentDialog: null
        }
      }
    },

    default: {
      ErrorComponents: {
        DialogError: {
          show: false,
          TitleDialog: null,
          TextContentDialog: null
        }
      }
    }
  },

  attributeUniversalComponents: {
    custom: {
      LoadingPages: {
        MyCustomLoadingResponse: {
          show: false
        }
      }
    }
  },

  attributePages: {
    dashboard: {
      AddProject: {
        DialogAddProyek: {
          show: false
        }
      }
    },
    MaterialPo: {
      indexArrayDataListMaterial: null,
      dataItemsTableGetMaterialPo: [],
      dataListMaterialGudang: [
        'Lokasi 1 A',
        'Lokasi 1 A1',
        'Lokasi 1 A2',
        'Lokasi 1 B',
        'Lokasi 1 B1',
        'Lokasi 1 B2',
        'Lokasi 1 C',
        'Lokasi 1 C1',
        'Lokasi 1 C2',
        'Lokasi 1 D',
        'Lokasi 1 D1',
        'Lokasi 1 D2',
        'Lokasi 1 E',
        'Lokasi 1 E1',
        'Lokasi 1 E2',
        'Lokasi 1 F',
        'Lokasi 1 F1',
        'Lokasi 1 F2',
        'Lokasi 1 G',
        'Lokasi 1 G1',
        'Lokasi 1 G2',
        'Lokasi 1 H',
        'Lokasi 1 H1',
        'Lokasi 1 H2',
        'Lokasi 1 I',
        'Lokasi 1 I1',
        'Lokasi 1 I2',
        'Lokasi 1 J',
        'Lokasi 1 J1',
        'Lokasi 1 J2',
        'Lokasi 1 K',
        'Lokasi 1 K1',
        'Lokasi 1 K2',
        'Lokasi 2 A',
        'Lokasi 2 A1',
        'Lokasi 2 A2',
        'Lokasi 2 B',
        'Lokasi 2 B1',
        'Lokasi 2 B2',
        'Lokasi 2 C',
        'Lokasi 2 C1',
        'Lokasi 2 C2',
        'Lokasi 2 D',
        'Lokasi 2 D1',
        'Lokasi 2 D2',
        'Lokasi 2 E',
        'Lokasi 2 E1',
        'Lokasi 2 E2',
        'Lokasi 2 F',
        'Lokasi 2 F1',
        'Lokasi 2 F2',
        'Lokasi 2 G',
        'Lokasi 2 G1',
        'Lokasi 2 G2',
        'Lokasi 2 H',
        'Lokasi 2 H1',
        'Lokasi 2 H2',
        'Lokasi 2 I',
        'Lokasi 2 I1',
        'Lokasi 2 I2',
        'Lokasi 2 J',
        'Lokasi 2 J1',
        'Lokasi 2 J2',
        'Lokasi 2 K',
        'Lokasi 2 K1',
        'Lokasi 2 K2',
        'Lokasi 3 A',
        'Lokasi 3 A1',
        'Lokasi 3 A2',
        'Lokasi 3 B',
        'Lokasi 3 B1',
        'Lokasi 3 B2',
        'Lokasi 3 C',
        'Lokasi 3 C1',
        'Lokasi 3 C2',
        'Lokasi 3 D',
        'Lokasi 3 D1',
        'Lokasi 3 D2',
        'Lokasi 3 E',
        'Lokasi 3 E1',
        'Lokasi 3 E2',
        'Lokasi 3 F',
        'Lokasi 3 F1',
        'Lokasi 3 F2',
        'Lokasi 3 G',
        'Lokasi 3 G1',
        'Lokasi 3 G2',
        'Lokasi 3 H',
        'Lokasi 3 H1',
        'Lokasi 3 H2',
        'Lokasi 3 I',
        'Lokasi 3 I1',
        'Lokasi 3 I2',
        'Lokasi 3 J',
        'Lokasi 3 J1',
        'Lokasi 3 J2',
        'Lokasi 3 K',
        'Lokasi 3 K1',
        'Lokasi 3 K2',
        'Lokasi 4 A',
        'Lokasi 4 A1',
        'Lokasi 4 A2',
        'Lokasi 4 B',
        'Lokasi 4 B1',
        'Lokasi 4 B2',
        'Lokasi 4 C',
        'Lokasi 4 C1',
        'Lokasi 4 C2',
        'Lokasi 4 D',
        'Lokasi 4 D1',
        'Lokasi 4 D2',
        'Lokasi 4 E',
        'Lokasi 4 E1',
        'Lokasi 4 E2',
        'Lokasi 4 F',
        'Lokasi 4 F1',
        'Lokasi 4 F2',
        'Lokasi 4 G',
        'Lokasi 4 G1',
        'Lokasi 4 G2',
        'Lokasi 4 H',
        'Lokasi 4 H1',
        'Lokasi 4 H2',
        'Lokasi 4 I',
        'Lokasi 4 I1',
        'Lokasi 4 I2',
        'Lokasi 4 J',
        'Lokasi 4 J1',
        'Lokasi 4 J2',
        'Lokasi 4 K',
        'Lokasi 4 K1',
        'Lokasi 4 K2',
        'Lokasi 5 A',
        'Lokasi 5 A1',
        'Lokasi 5 A2',
        'Lokasi 5 B',
        'Lokasi 5 B1',
        'Lokasi 5 B2',
        'Lokasi 5 C',
        'Lokasi 5 C1',
        'Lokasi 5 C2',
        'Lokasi 5 D',
        'Lokasi 5 D1',
        'Lokasi 5 D2',
        'Lokasi 5 E',
        'Lokasi 5 E1',
        'Lokasi 5 E2',
        'Lokasi 5 F',
        'Lokasi 5 F1',
        'Lokasi 5 F2',
        'Lokasi 5 G',
        'Lokasi 5 G1',
        'Lokasi 5 G2',
        'Lokasi 5 H',
        'Lokasi 5 H1',
        'Lokasi 5 H2',
        'Lokasi 5 I',
        'Lokasi 5 I1',
        'Lokasi 5 I2',
        'Lokasi 5 J',
        'Lokasi 5 J1',
        'Lokasi 5 J2',
        'Lokasi 5 K',
        'Lokasi 5 K1',
        'Lokasi 5 K2',
        'Lokasi 6 A',
        'Lokasi 6 A1',
        'Lokasi 6 A2',
        'Lokasi 6 B',
        'Lokasi 6 B1',
        'Lokasi 6 B2',
        'Lokasi 6 C',
        'Lokasi 6 C1',
        'Lokasi 6 C2',
        'Lokasi 6 D',
        'Lokasi 6 D1',
        'Lokasi 6 D2',
        'Lokasi 6 E',
        'Lokasi 6 E1',
        'Lokasi 6 E2',
        'Lokasi 6 F',
        'Lokasi 6 F1',
        'Lokasi 6 F2',
        'Lokasi 6 G',
        'Lokasi 6 G1',
        'Lokasi 6 G2',
        'Lokasi 6 H',
        'Lokasi 6 H1',
        'Lokasi 6 H2',
        'Lokasi 6 I',
        'Lokasi 6 I1',
        'Lokasi 6 I2',
        'Lokasi 6 J',
        'Lokasi 6 J1',
        'Lokasi 6 J2',
        'Lokasi 6 K',
        'Lokasi 6 K1',
        'Lokasi 6 K2',
        'Lokasi 7 A',
        'Lokasi 7 A1',
        'Lokasi 7 A2',
        'Lokasi 7 B',
        'Lokasi 7 B1',
        'Lokasi 7 B2',
        'Lokasi 7 C',
        'Lokasi 7 C1',
        'Lokasi 7 C2',
        'Lokasi 7 D',
        'Lokasi 7 D1',
        'Lokasi 7 D2',
        'Lokasi 7 E',
        'Lokasi 7 E1',
        'Lokasi 7 E2',
        'Lokasi 7 F',
        'Lokasi 7 F1',
        'Lokasi 7 F2',
        'Lokasi 7 G',
        'Lokasi 7 G1',
        'Lokasi 7 G2',
        'Lokasi 7 H',
        'Lokasi 7 H1',
        'Lokasi 7 H2',
        'Lokasi 7 I',
        'Lokasi 7 I1',
        'Lokasi 7 I2',
        'Lokasi 7 J',
        'Lokasi 7 J1',
        'Lokasi 7 J2',
        'Lokasi 7 K',
        'Lokasi 7 K1',
        'Lokasi 7 K2',
        'Lokasi 8 A',
        'Lokasi 8 A1',
        'Lokasi 8 A2',
        'Lokasi 8 B',
        'Lokasi 8 B1',
        'Lokasi 8 B2',
        'Lokasi 8 C',
        'Lokasi 8 C1',
        'Lokasi 8 C2',
        'Lokasi 8 D',
        'Lokasi 8 D1',
        'Lokasi 8 D2',
        'Lokasi 8 E',
        'Lokasi 8 E1',
        'Lokasi 8 E2',
        'Lokasi 8 F',
        'Lokasi 8 F1',
        'Lokasi 8 F2',
        'Lokasi 8 G',
        'Lokasi 8 G1',
        'Lokasi 8 G2',
        'Lokasi 8 H',
        'Lokasi 8 H1',
        'Lokasi 8 H2',
        'Lokasi 8 I',
        'Lokasi 8 I1',
        'Lokasi 8 I2',
        'Lokasi 8 J',
        'Lokasi 8 J1',
        'Lokasi 8 J2',
        'Lokasi 8 K',
        'Lokasi 8 K1',
        'Lokasi 8 K2',
        'Lokasi 9 A',
        'Lokasi 9 A1',
        'Lokasi 9 A2',
        'Lokasi 9 B',
        'Lokasi 9 B1',
        'Lokasi 9 B2',
        'Lokasi 9 C',
        'Lokasi 9 C1',
        'Lokasi 9 C2',
        'Lokasi 9 D',
        'Lokasi 9 D1',
        'Lokasi 9 D2',
        'Lokasi 9 E',
        'Lokasi 9 E1',
        'Lokasi 9 E2',
        'Lokasi 9 F',
        'Lokasi 9 F1',
        'Lokasi 9 F2',
        'Lokasi 9 G',
        'Lokasi 9 G1',
        'Lokasi 9 G2',
        'Lokasi 9 H',
        'Lokasi 9 H1',
        'Lokasi 9 H2',
        'Lokasi 9 I',
        'Lokasi 9 I1',
        'Lokasi 9 I2',
        'Lokasi 9 J',
        'Lokasi 9 J1',
        'Lokasi 9 J2',
        'Lokasi 9 K',
        'Lokasi 9 K1',
        'Lokasi 9 K2',
        'Lokasi SB A',
        'Lokasi SB A1',
        'Lokasi SB A2',
        'Lokasi SB B',
        'Lokasi SB B1',
        'Lokasi SB B2',
        'Lokasi SB C',
        'Lokasi SB C1',
        'Lokasi SB C2',
        'Lokasi SB D',
        'Lokasi SB D1',
        'Lokasi SB D2',
        'Lokasi SB E',
        'Lokasi SB E1',
        'Lokasi SB E2',
        'Lokasi SB F',
        'Lokasi SB F1',
        'Lokasi SB F2',
        'Lokasi SB G',
        'Lokasi SB G1',
        'Lokasi SB G2',
        'Lokasi SB H',
        'Lokasi SB H1',
        'Lokasi SB H2',
        'Lokasi PJ1 ',
        'Lokasi PJ2',
        'RECOIL E47',
        'Lokasi Profile CD 35',
        'Lokasi Profile DE 35',
        'Lokasi Profile DE 36 ',
        'Lokasi Profile E47'
      ],
      dialogSelectMaterialForSetDataDetailMaterial: {
        show: false,
        dataItemsTableSetDetailDataMaterialPo: []
      },
      dialogSetDetailDataMaterialPo: {
        show: false,
        dataDetailMaterial: []
      },
      dialogCreateNewMvir: {
        show: false,
        dataItemsSelectMvir: ['Buat MVIR baru']
      }
    },
    ReportMvir: {
      dialogPrintMvir: {
        show: false,
        dataHeaderMvir: [],
        namaProject: null,
        dataMvir: [],
        dataMaterialByMvir: [],
        hideInputEditDataMvir: {
          millCertNo: [],
          visual: []
        },
        dataArrayTonageMaterial: [],
        dataArrayQtyMaterial: []
      }
    },
    MaterialCus: {
      reloadGetDataMaterial: null,
      indexArrayDataListMaterial: null,
      dataItemsTableGetMaterialCus: [],
      dataListMaterialGudang: [
        'Lokasi 1 A',
        'Lokasi 1 A1',
        'Lokasi 1 A2',
        'Lokasi 1 B',
        'Lokasi 1 B1',
        'Lokasi 1 B2',
        'Lokasi 1 C',
        'Lokasi 1 C1',
        'Lokasi 1 C2',
        'Lokasi 1 D',
        'Lokasi 1 D1',
        'Lokasi 1 D2',
        'Lokasi 1 E',
        'Lokasi 1 E1',
        'Lokasi 1 E2',
        'Lokasi 1 F',
        'Lokasi 1 F1',
        'Lokasi 1 F2',
        'Lokasi 1 G',
        'Lokasi 1 G1',
        'Lokasi 1 G2',
        'Lokasi 1 H',
        'Lokasi 1 H1',
        'Lokasi 1 H2',
        'Lokasi 1 I',
        'Lokasi 1 I1',
        'Lokasi 1 I2',
        'Lokasi 1 J',
        'Lokasi 1 J1',
        'Lokasi 1 J2',
        'Lokasi 1 K',
        'Lokasi 1 K1',
        'Lokasi 1 K2',
        'Lokasi 2 A',
        'Lokasi 2 A1',
        'Lokasi 2 A2',
        'Lokasi 2 B',
        'Lokasi 2 B1',
        'Lokasi 2 B2',
        'Lokasi 2 C',
        'Lokasi 2 C1',
        'Lokasi 2 C2',
        'Lokasi 2 D',
        'Lokasi 2 D1',
        'Lokasi 2 D2',
        'Lokasi 2 E',
        'Lokasi 2 E1',
        'Lokasi 2 E2',
        'Lokasi 2 F',
        'Lokasi 2 F1',
        'Lokasi 2 F2',
        'Lokasi 2 G',
        'Lokasi 2 G1',
        'Lokasi 2 G2',
        'Lokasi 2 H',
        'Lokasi 2 H1',
        'Lokasi 2 H2',
        'Lokasi 2 I',
        'Lokasi 2 I1',
        'Lokasi 2 I2',
        'Lokasi 2 J',
        'Lokasi 2 J1',
        'Lokasi 2 J2',
        'Lokasi 2 K',
        'Lokasi 2 K1',
        'Lokasi 2 K2',
        'Lokasi 3 A',
        'Lokasi 3 A1',
        'Lokasi 3 A2',
        'Lokasi 3 B',
        'Lokasi 3 B1',
        'Lokasi 3 B2',
        'Lokasi 3 C',
        'Lokasi 3 C1',
        'Lokasi 3 C2',
        'Lokasi 3 D',
        'Lokasi 3 D1',
        'Lokasi 3 D2',
        'Lokasi 3 E',
        'Lokasi 3 E1',
        'Lokasi 3 E2',
        'Lokasi 3 F',
        'Lokasi 3 F1',
        'Lokasi 3 F2',
        'Lokasi 3 G',
        'Lokasi 3 G1',
        'Lokasi 3 G2',
        'Lokasi 3 H',
        'Lokasi 3 H1',
        'Lokasi 3 H2',
        'Lokasi 3 I',
        'Lokasi 3 I1',
        'Lokasi 3 I2',
        'Lokasi 3 J',
        'Lokasi 3 J1',
        'Lokasi 3 J2',
        'Lokasi 3 K',
        'Lokasi 3 K1',
        'Lokasi 3 K2',
        'Lokasi 4 A',
        'Lokasi 4 A1',
        'Lokasi 4 A2',
        'Lokasi 4 B',
        'Lokasi 4 B1',
        'Lokasi 4 B2',
        'Lokasi 4 C',
        'Lokasi 4 C1',
        'Lokasi 4 C2',
        'Lokasi 4 D',
        'Lokasi 4 D1',
        'Lokasi 4 D2',
        'Lokasi 4 E',
        'Lokasi 4 E1',
        'Lokasi 4 E2',
        'Lokasi 4 F',
        'Lokasi 4 F1',
        'Lokasi 4 F2',
        'Lokasi 4 G',
        'Lokasi 4 G1',
        'Lokasi 4 G2',
        'Lokasi 4 H',
        'Lokasi 4 H1',
        'Lokasi 4 H2',
        'Lokasi 4 I',
        'Lokasi 4 I1',
        'Lokasi 4 I2',
        'Lokasi 4 J',
        'Lokasi 4 J1',
        'Lokasi 4 J2',
        'Lokasi 4 K',
        'Lokasi 4 K1',
        'Lokasi 4 K2',
        'Lokasi 5 A',
        'Lokasi 5 A1',
        'Lokasi 5 A2',
        'Lokasi 5 B',
        'Lokasi 5 B1',
        'Lokasi 5 B2',
        'Lokasi 5 C',
        'Lokasi 5 C1',
        'Lokasi 5 C2',
        'Lokasi 5 D',
        'Lokasi 5 D1',
        'Lokasi 5 D2',
        'Lokasi 5 E',
        'Lokasi 5 E1',
        'Lokasi 5 E2',
        'Lokasi 5 F',
        'Lokasi 5 F1',
        'Lokasi 5 F2',
        'Lokasi 5 G',
        'Lokasi 5 G1',
        'Lokasi 5 G2',
        'Lokasi 5 H',
        'Lokasi 5 H1',
        'Lokasi 5 H2',
        'Lokasi 5 I',
        'Lokasi 5 I1',
        'Lokasi 5 I2',
        'Lokasi 5 J',
        'Lokasi 5 J1',
        'Lokasi 5 J2',
        'Lokasi 5 K',
        'Lokasi 5 K1',
        'Lokasi 5 K2',
        'Lokasi 6 A',
        'Lokasi 6 A1',
        'Lokasi 6 A2',
        'Lokasi 6 B',
        'Lokasi 6 B1',
        'Lokasi 6 B2',
        'Lokasi 6 C',
        'Lokasi 6 C1',
        'Lokasi 6 C2',
        'Lokasi 6 D',
        'Lokasi 6 D1',
        'Lokasi 6 D2',
        'Lokasi 6 E',
        'Lokasi 6 E1',
        'Lokasi 6 E2',
        'Lokasi 6 F',
        'Lokasi 6 F1',
        'Lokasi 6 F2',
        'Lokasi 6 G',
        'Lokasi 6 G1',
        'Lokasi 6 G2',
        'Lokasi 6 H',
        'Lokasi 6 H1',
        'Lokasi 6 H2',
        'Lokasi 6 I',
        'Lokasi 6 I1',
        'Lokasi 6 I2',
        'Lokasi 6 J',
        'Lokasi 6 J1',
        'Lokasi 6 J2',
        'Lokasi 6 K',
        'Lokasi 6 K1',
        'Lokasi 6 K2',
        'Lokasi 7 A',
        'Lokasi 7 A1',
        'Lokasi 7 A2',
        'Lokasi 7 B',
        'Lokasi 7 B1',
        'Lokasi 7 B2',
        'Lokasi 7 C',
        'Lokasi 7 C1',
        'Lokasi 7 C2',
        'Lokasi 7 D',
        'Lokasi 7 D1',
        'Lokasi 7 D2',
        'Lokasi 7 E',
        'Lokasi 7 E1',
        'Lokasi 7 E2',
        'Lokasi 7 F',
        'Lokasi 7 F1',
        'Lokasi 7 F2',
        'Lokasi 7 G',
        'Lokasi 7 G1',
        'Lokasi 7 G2',
        'Lokasi 7 H',
        'Lokasi 7 H1',
        'Lokasi 7 H2',
        'Lokasi 7 I',
        'Lokasi 7 I1',
        'Lokasi 7 I2',
        'Lokasi 7 J',
        'Lokasi 7 J1',
        'Lokasi 7 J2',
        'Lokasi 7 K',
        'Lokasi 7 K1',
        'Lokasi 7 K2',
        'Lokasi 8 A',
        'Lokasi 8 A1',
        'Lokasi 8 A2',
        'Lokasi 8 B',
        'Lokasi 8 B1',
        'Lokasi 8 B2',
        'Lokasi 8 C',
        'Lokasi 8 C1',
        'Lokasi 8 C2',
        'Lokasi 8 D',
        'Lokasi 8 D1',
        'Lokasi 8 D2',
        'Lokasi 8 E',
        'Lokasi 8 E1',
        'Lokasi 8 E2',
        'Lokasi 8 F',
        'Lokasi 8 F1',
        'Lokasi 8 F2',
        'Lokasi 8 G',
        'Lokasi 8 G1',
        'Lokasi 8 G2',
        'Lokasi 8 H',
        'Lokasi 8 H1',
        'Lokasi 8 H2',
        'Lokasi 8 I',
        'Lokasi 8 I1',
        'Lokasi 8 I2',
        'Lokasi 8 J',
        'Lokasi 8 J1',
        'Lokasi 8 J2',
        'Lokasi 8 K',
        'Lokasi 8 K1',
        'Lokasi 8 K2',
        'Lokasi 9 A',
        'Lokasi 9 A1',
        'Lokasi 9 A2',
        'Lokasi 9 B',
        'Lokasi 9 B1',
        'Lokasi 9 B2',
        'Lokasi 9 C',
        'Lokasi 9 C1',
        'Lokasi 9 C2',
        'Lokasi 9 D',
        'Lokasi 9 D1',
        'Lokasi 9 D2',
        'Lokasi 9 E',
        'Lokasi 9 E1',
        'Lokasi 9 E2',
        'Lokasi 9 F',
        'Lokasi 9 F1',
        'Lokasi 9 F2',
        'Lokasi 9 G',
        'Lokasi 9 G1',
        'Lokasi 9 G2',
        'Lokasi 9 H',
        'Lokasi 9 H1',
        'Lokasi 9 H2',
        'Lokasi 9 I',
        'Lokasi 9 I1',
        'Lokasi 9 I2',
        'Lokasi 9 J',
        'Lokasi 9 J1',
        'Lokasi 9 J2',
        'Lokasi 9 K',
        'Lokasi 9 K1',
        'Lokasi 9 K2',
        'Lokasi SB A',
        'Lokasi SB A1',
        'Lokasi SB A2',
        'Lokasi SB B',
        'Lokasi SB B1',
        'Lokasi SB B2',
        'Lokasi SB C',
        'Lokasi SB C1',
        'Lokasi SB C2',
        'Lokasi SB D',
        'Lokasi SB D1',
        'Lokasi SB D2',
        'Lokasi SB E',
        'Lokasi SB E1',
        'Lokasi SB E2',
        'Lokasi SB F',
        'Lokasi SB F1',
        'Lokasi SB F2',
        'Lokasi SB G',
        'Lokasi SB G1',
        'Lokasi SB G2',
        'Lokasi SB H',
        'Lokasi SB H1',
        'Lokasi SB H2',
        'Lokasi PJ1 ',
        'Lokasi PJ2',
        'RECOIL E47',
        'Lokasi Profile CD 35',
        'Lokasi Profile DE 35',
        'Lokasi Profile DE 36 ',
        'Lokasi Profile E47'
      ],
      dialogSetDetailDataMaterialCus: {
        show: false,
        dataDetailMaterial: []
      },
      dialogCreateNewMvir: {
        show: false,
        dataItemsSelectMvir: ['Buat MVIR baru']
      }
    }
  }
}
