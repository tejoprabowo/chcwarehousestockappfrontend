export default {
  SET_AUTH_USER_LOGIN(state, token) {
    state.attributeAuthentication.tokenUser = token
    state.attributeAuthentication.login = true
  },

  SET_INFORMATION_USER_LOGIN(state, dataUser) {
    state.attributeAuthentication.username = dataUser.username
    state.attributeAuthentication.roleUser = dataUser.role_user
  },

  SET_LOGOUT_APP(state) {
    state.attributeAuthentication.tokenUser = null
    state.attributeAuthentication.login = false
    state.attributeAuthentication.username = null
    state.attributeAuthentication.roleUser = null
  }
}
