import Cookies from 'js-cookie'
import cookie from 'cookie'

export default {
  async nuxtServerInit({ commit, getters }, context) {
    try {
      const cookies = cookie.parse(context.req.headers.cookie)
      commit('SET_AUTH_USER_LOGIN', cookies.token)
      try {
        const { data } = await this.$axios.get('checkToken', getters.tokenUser)
        commit('SET_INFORMATION_USER_LOGIN', data)
      } catch (e) {
        commit('SET_LOGOUT_APP')
      }
    } catch (e) {
      commit('SET_LOGOUT_APP')
    }
  },

  async checkUsernamePassword({ commit, state, getters }, { email, password }) {
    try {
      const { data } = await this.$axios.post('login', {
        email,
        password
      })
      Cookies.set('token', data.dataToken.token)
      commit('SET_AUTH_USER_LOGIN', data.dataToken.token)
      try {
        const { data } = await this.$axios.get('checkToken', getters.tokenUser)
        commit('SET_INFORMATION_USER_LOGIN', data)
        this.$router.push('/dashboard')
      } catch (e) {
        commit('SET_LOGOUT_APP')
      }
      state.attributeUniversalComponents.custom.LoadingPages.MyCustomLoadingResponse.show = false
    } catch (e) {
      state.attributeUniversalComponents.custom.LoadingPages.MyCustomLoadingResponse.show = false
      state.attributeLayouts.login.ErrorComponents.DialogError.show = true
      state.attributeLayouts.login.ErrorComponents.DialogError.TitleDialog =
        'Terjadi kesalahan sistem!'
      state.attributeLayouts.login.ErrorComponents.DialogError.TextContentDialog =
        'Sepertinya terjadi error / bug pada sistem login, harap anda hubungi web developer melalui WA: +6281237594133 atau email: tejoprabowo4@gmail.com untuk memperbaiki kesalahan ini.'
      try {
        state.attributeUniversalComponents.custom.LoadingPages.MyCustomLoadingResponse.show = false
        e.response.status
        state.attributeLayouts.login.ErrorComponents.DialogError.show = true
        state.attributeLayouts.login.ErrorComponents.DialogError.TitleDialog =
          e.response.data.titleErr
        state.attributeLayouts.login.ErrorComponents.DialogError.TextContentDialog =
          e.response.data.textContentErr
      } catch (e) {
        state.attributeUniversalComponents.custom.LoadingPages.MyCustomLoadingResponse.show = false
        // console.clear()
      }
    }
  },

  setLoadingResponseServer({ state }, { requestStatus }) {
    if (!requestStatus) {
      if (
        state.attributeUniversalComponents.custom.LoadingPages
          .MyCustomLoadingResponse.show
      ) {
        return (state.attributeUniversalComponents.custom.LoadingPages.MyCustomLoadingResponse.show = false)
      }
    } else {
      if (
        !state.attributeUniversalComponents.custom.LoadingPages
          .MyCustomLoadingResponse.show
      ) {
        return (state.attributeUniversalComponents.custom.LoadingPages.MyCustomLoadingResponse.show = true)
      }
    }
  }
}
