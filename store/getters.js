export default {
  tokenUser(state) {
    return {
      headers: {
        Authorization: 'Bearer ' + state.attributeAuthentication.tokenUser
      }
    }
  }
}
