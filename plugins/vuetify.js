import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify, {
  theme: {
    primary: '#D32F2F',
    secondary: '#EF5350',
    accent: '#CE93D8',
    error: '#E65100',
    warning: '#FDD835',
    info: '#2196f3',
    success: '#4caf50'
  }
})
