export default function({ store, route, redirect }) {
  if (!store.state.attributeAuthentication.login) {
    redirect('/')
    if (route.path !== '/') {
      store.state.attributeLayouts.login.ErrorComponents.DialogError.show = true
      store.state.attributeLayouts.login.ErrorComponents.DialogError.TitleDialog =
        'Session Login Habis'
      store.state.attributeLayouts.login.ErrorComponents.DialogError.TextContentDialog =
        'Session login anda sudah habis mohon login ulang untuk memakai aplikasi'
      setTimeout(() => {
        store.state.attributeLayouts.login.ErrorComponents.DialogError.show = false
        store.state.attributeLayouts.login.ErrorComponents.DialogError.TitleDialog = null
        store.state.attributeLayouts.login.ErrorComponents.DialogError.TextContentDialog = null
      }, 1000)
    }
  }
}
