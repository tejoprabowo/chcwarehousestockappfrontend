export default function({ store, redirect }) {
  if (store.state.attributeAuthentication.login) {
    redirect('/dashboard')
    store.state.attributeLayouts.default.ErrorComponents.DialogError.show = true
    store.state.attributeLayouts.default.ErrorComponents.DialogError.TitleDialog =
      'Halaman tidak bisa di akses!'
    store.state.attributeLayouts.default.ErrorComponents.DialogError.TextContentDialog =
      'Halaman login tidak bisa di akses karna anda sudah login!'
    setTimeout(() => {
      store.state.attributeLayouts.default.ErrorComponents.DialogError.show = false
      store.state.attributeLayouts.default.ErrorComponents.DialogError.TitleDialog = null
      store.state.attributeLayouts.default.ErrorComponents.DialogError.TextContentDialog = null
    }, 1000)
  }
}
